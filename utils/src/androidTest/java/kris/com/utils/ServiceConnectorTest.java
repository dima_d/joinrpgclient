package kris.com.utils;

import android.support.test.InstrumentationRegistry;
import android.support.test.runner.AndroidJUnit4;
import android.util.Log;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import kris.com.utils.service.BoundService;
import kris.com.utils.service.IServiceConnector;

@RunWith(AndroidJUnit4.class)
public class ServiceConnectorTest {

    private static final String TAG = ServiceConnectorTest.class.getSimpleName();

    @Before
    public void setUp() throws Exception {


    }

    @After
    public void tearDown() throws Exception {

    }

    @Test
    public void getService() throws Exception {
        Log.d(TAG, "getService() called");
        IServiceConnector<BoundService> serviceConnector = BoundService.connector(InstrumentationRegistry.getTargetContext(), BoundService.class);

        serviceConnector.getService()
                .test()
                .assertValueCount(1)
                .assertNoErrors()
                .assertComplete();

        serviceConnector.complete();

        serviceConnector.getService()
                .test()
                .assertNoValues()
                .assertNoErrors()
                .assertComplete();

    }
}
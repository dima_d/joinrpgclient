package kris.com.utils.presenter;

/**
 * Interface provide RxLifecycle. Observables will be complete, while stpo method is called.<br/>
 * Usage : Observable.compose(bindToLifecycle()).subscribe();<br/><br/>
 * Created by dmitry subbotenko on 05.05.2017.
 */

public interface ILifeCycle {

  /**
   * Lifecycle transformer to complete observer while stop();<br/>
   * Usage : Observable.compose(bindToLifecycle()).subscribe();<br/>
   * @return transformer
   */
  <T> LifeCycle.LifecycleTransformer<T> bindToLifecycle();

  /**
   * Stop aff lifecycle observables.
   */
  void stop();
}

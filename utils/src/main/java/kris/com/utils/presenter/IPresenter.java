package kris.com.utils.presenter;

public interface IPresenter extends ILifeCycle {
    <T, _VIEW> Presenter.LifecycleViewTransformer<T,_VIEW> bindToViewLifecycle(_VIEW view);
    <_VIEW> void unbindView(_VIEW view);
}

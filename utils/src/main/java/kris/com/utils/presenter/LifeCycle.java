package kris.com.utils.presenter;

import io.reactivex.Observable;
import io.reactivex.ObservableSource;
import io.reactivex.ObservableTransformer;
import io.reactivex.Single;
import io.reactivex.SingleSource;
import io.reactivex.SingleTransformer;
import io.reactivex.subjects.BehaviorSubject;

public class LifeCycle implements ILifeCycle {
  protected BehaviorSubject<Boolean> lifecycleSubject = BehaviorSubject.create();

  @Override
  public final <T> LifecycleTransformer<T> bindToLifecycle() {
    return new LifecycleTransformer<T>();
  }

  @Override
  public void stop() {
    lifecycleSubject.onNext(true);
    lifecycleSubject.onComplete();
  }


  public class LifecycleTransformer<T> implements ObservableTransformer<T, T>,
          SingleTransformer<T, T> {

    @Override
    public ObservableSource<T> apply(Observable<T> upstream) {
      return upstream.takeUntil(lifecycleSubject);
    }

    @Override
    public SingleSource<T> apply(Single<T> upstream) {
      return upstream.takeUntil(lifecycleSubject.firstOrError());
    }
  }

}

package kris.com.utils.presenter;

import io.reactivex.Observable;
import io.reactivex.ObservableSource;
import io.reactivex.ObservableTransformer;
import io.reactivex.subjects.PublishSubject;

public abstract class Presenter extends LifeCycle implements IPresenter {
    private PublishSubject<Object> lifecycleViewSubject = PublishSubject.create();

    @Override
    public final <T, _VIEW>  LifecycleViewTransformer<T,_VIEW> bindToViewLifecycle(_VIEW view) {
        return new LifecycleViewTransformer<T, _VIEW>(view);
    }

    @Override
    public <_VIEW> void unbindView(_VIEW view) {
        lifecycleViewSubject.onNext(view);
    }

    @Override
    public void stop() {
        super.stop();
        lifecycleViewSubject.onComplete();
    }

    protected class LifecycleViewTransformer<T,_VIEW> implements ObservableTransformer<T, T> {
        private _VIEW view;

        private LifecycleViewTransformer(_VIEW view) {
            super();
            this.view = view;
        }

        @Override
        public ObservableSource<T> apply(Observable<T> upstream) {
            return upstream
                    .takeUntil(lifecycleSubject)
                    .takeUntil(lifecycleViewSubject
                            .filter(o -> view == o)
                    );
        }
    }
}

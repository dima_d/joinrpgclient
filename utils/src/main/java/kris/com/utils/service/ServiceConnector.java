package kris.com.utils.service;

import android.app.Service;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.IBinder;

import io.reactivex.Observable;
import io.reactivex.ObservableTransformer;
import io.reactivex.subjects.BehaviorSubject;

/**
 * Connector base class to support service connection.
 */
class ServiceConnector<T extends BoundService> implements IServiceConnector<T> {

    private BehaviorSubject<T> serviceSubject = BehaviorSubject.create();
    private BoundServiceConnection boundServiceConnection = new BoundServiceConnection();

    /**
     * Base class constructor.<br/><br/>
     * <p>
     * Returns an object, that's provide access to service.<br/>
     *
     * @param context   Context ot service binding.
     * @param clazz     Class of the service
     */
    ServiceConnector(Context context, Class<T> clazz) {
        this(context, clazz, null);
    }

    /**
     * Base class constructor.<br/><br/>
     * <p>
     * Returns an object, that's provide access to service.<br/>
     *
     * @param context   Context ot service binding.
     * @param clazz     Class of the service
     * @param lifeCycle Observable transformer, that stop binding service, while it is not needed
     *                  You can use {@link kris.com.utils.presenter.LifeCycle} class from this package, or trello/RxLifecycle package.
     */
    ServiceConnector(Context context, Class<T> clazz, ObservableTransformer<T, T> lifeCycle) {

        if (lifeCycle == null) {
            lifeCycle = t -> t;
        }

        Intent orderServiceIntent = new Intent(context, clazz);
        context.bindService(orderServiceIntent,
                boundServiceConnection, Context.BIND_AUTO_CREATE);

        serviceSubject
                .compose(lifeCycle)
                .doOnError(Throwable::printStackTrace)
                .onErrorResumeNext(t -> {
                })
                .doOnComplete(() -> {
                    context.unbindService(boundServiceConnection);
                })
                .subscribe();
    }

    /**
     * Return Observable that return just 1 instance of the service, and complete.
     * @return observable of service.
     */
    @Override
    public Observable<T> getService() {
        return serviceSubject.take(1);
    }

    /**
     * Unbind service, stop serviceConnector and complete it. This object can't be set again.
     */
    @Override
    public void complete() {
        serviceSubject.onComplete();
    }

    private class BoundServiceConnection implements ServiceConnection {

        @Override
        public void onServiceConnected(ComponentName name, IBinder binder) {
            Service service = ((ServiceBinder) binder).getService();
            serviceSubject.onNext((T) service); // May produce Class cast Exception
        }
        @Override
        public void onServiceDisconnected(ComponentName name) {
            serviceSubject.onComplete();
        }

    }
}
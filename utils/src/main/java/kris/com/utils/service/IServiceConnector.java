package kris.com.utils.service;

import io.reactivex.Observable;

public interface IServiceConnector<T extends BoundService> {

    Observable<T> getService();

    void complete();
}

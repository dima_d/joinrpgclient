package kris.com.utils.service;

import android.os.Binder;

/**
 * Created by Dmitry Subbotenko on 28.02.2017.
 */
class ServiceBinder extends Binder {

    private BoundService service;

    ServiceBinder(BoundService service) {
        this.service = service;
    }

    BoundService getService() {
        return service;
    }
}

package kris.com.utils.service;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.IBinder;

import io.reactivex.ObservableTransformer;

/**
 * Created by Dmitry Subbotenko on 28.02.2017.
 */
public class BoundService extends Service {
    private IBinder binder = new ServiceBinder(this);

    @Override
    public IBinder onBind(Intent intent) {
        return binder;
    }

    public static <T extends BoundService> IServiceConnector<T> connector(Context context, Class<T> service){
        return new ServiceConnector<T>(context, service);
    }

    public static <T extends BoundService> IServiceConnector<T> connector(Context context, Class<T> service, ObservableTransformer<T,T> lifecycle){
        return new ServiceConnector<T>(context, service, lifecycle);
    }


}

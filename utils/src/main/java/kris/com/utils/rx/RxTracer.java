package kris.com.utils.rx;

import android.util.Log;

import io.reactivex.Observable;
import io.reactivex.ObservableOperator;
import io.reactivex.ObservableSource;
import io.reactivex.ObservableTransformer;
import io.reactivex.Observer;
import io.reactivex.annotations.NonNull;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Consumer;

public class RxTracer<T> implements ObservableOperator<T, T> {

    private static final String TAG = RxTracer.class.getSimpleName();

    private final StackTraceElement[] trace = new Throwable().getStackTrace();

    public static <T> RxTracer<T> traceOnError() {
        return new RxTracer<>();
    }

    public static Consumer<Throwable> onErrorLogger() {
        return new ThrowableLogger();
    }

    public static <R> LoggerTransformer<R> log(String tag, String msg) {
        return new LoggerTransformer<R>(tag, msg);
    }



    @Override
    public Observer<? super T> apply(Observer<? super T> child) {

        return new Observer<T>() {
            @Override
            public void onSubscribe(Disposable d) {
                child.onSubscribe(d);
            }

            @Override
            public void onNext(T t) {
                child.onNext(t);
            }

            @Override
            public void onError(Throwable e) {
                addStackTrace(e, trace);
                child.onError(e);
            }

            @Override
            public void onComplete() {
                child.onComplete();
            }
        };
    }

    private static void addStackTrace(Throwable e, StackTraceElement[] trace) {
        try {
            StackTraceElement[] oldStackTrace = e.getStackTrace();
            StackTraceElement[] newStackTrace = new StackTraceElement[trace.length + oldStackTrace.length];
            System.arraycopy(trace, 0, newStackTrace, 0, trace.length);
            System.arraycopy(oldStackTrace, 0, newStackTrace, trace.length, oldStackTrace.length);
            e.setStackTrace(newStackTrace);
        } catch (NullPointerException npe) { // Error in HTC. Internal NPE, if stack trace is null.
            // do nothing.
        }
    }

    private static class LoggerTransformer<R> implements ObservableTransformer<R, R> {

        private String tag;
        private String msg;

        LoggerTransformer(String tag, String msg) {
            this.tag = tag;
            this.msg = msg;
        }

        @Override
        public ObservableSource<R> apply(Observable<R> upstream) {
            return upstream
                    .doOnNext(l -> Log.d(tag, msg + System.identityHashCode(upstream)+ " onNext:" + l))
                    .doOnError(l -> Log.d(tag, msg  + System.identityHashCode(upstream)+ " onError:" + l))
                    .doOnComplete(() -> Log.d(tag, msg  + System.identityHashCode(upstream)+ " onComplete:"))
                    .doOnSubscribe(l -> Log.d(tag, msg  + System.identityHashCode(upstream)+ " onSubscribe:" + l))
                    .doOnTerminate(() -> Log.d(tag, msg  + System.identityHashCode(upstream)+ " doOnTerminate:"))
                    .doOnDispose(() -> Log.d(tag, msg  + System.identityHashCode(upstream)+ " doOnDispose:"))
                    ;
        }
    }

    private static class ThrowableLogger implements Consumer<Throwable> {

        private final StackTraceElement[] trace = new Throwable().getStackTrace();

        @Override
        public void accept(@NonNull Throwable throwable) throws Exception {
            addStackTrace(throwable, trace);
            throwable.printStackTrace();
        }
    }

}
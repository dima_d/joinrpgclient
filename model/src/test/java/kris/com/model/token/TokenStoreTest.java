package kris.com.model.token;

import org.junit.Before;
import org.junit.Test;

import io.reactivex.Observable;
import kris.com.model.network.beans.Token;
import kris.com.model.network.beans.User;

public class TokenStoreTest {

    private TokenStore tokenStore;
    private int i = 0;

    @Before
    public void setUp() throws Exception {
        tokenStore = new TokenStore((username, password, grant_type) -> {
            i++;
            return Observable.just(new Token(String.valueOf(i), "", ""));
        }, new User("", ""));
    }

    @Test
    public void tokenSender() throws Exception {
        System.out.println("tokenSender()");
        tokenStore.tokenSender(Observable::just).test().assertValue("1").assertNoErrors().assertComplete();
        tokenStore.tokenSender(Observable::just).test().assertValue("1").assertNoErrors().assertComplete();
        tokenStore.tokenSender(Observable::just).test().assertValue("1").assertNoErrors().assertComplete();

        tokenStore.tokenSender(s -> {
            if (!s.equals("2")) return Observable.error(new Exception());
            return Observable.just(s);
        })
                .test()
                .assertValue("2")
                .assertNoErrors()
                .assertComplete();

        tokenStore.tokenSender(Observable::just).test().assertValue("2").assertNoErrors().assertComplete();

        tokenStore.tokenSender(s -> Observable.error(new Exception()))
                .test()
                .assertError(Exception.class);

        tokenStore.tokenSender(Observable::just).test().assertValue("4").assertNoErrors().assertComplete();

    }
}
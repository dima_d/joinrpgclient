package kris.com.model.network;

import android.support.test.runner.AndroidJUnit4;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.Date;

import kris.com.model.network.beans.User;
import kris.com.model.token.TokenStore;

@RunWith(AndroidJUnit4.class)
public class JoinRpgApiTest {

    private JoinRpgRetrofit rpgRetrofit;
    private User user;

    @Before
    public void setUp() throws Exception {

        user = new User("bi-tz@ya.ru", "квантсщь");
        RestParameters restParameters =  new RestParameters(
                user,
                "http://dev.joinrpg.ru"
        );

        rpgRetrofit = new RetrofitServiceFactory<>(restParameters, JoinRpgRetrofit.class).create();
    }

    @Test
    public void apiTest() throws Exception {

        TokenStore tokenStore = new TokenStore(rpgRetrofit::getToken, user);

        tokenStore.tokenSender(token -> rpgRetrofit.getCheckins(token, 151)).test()
                .await()
                .assertNoErrors()
                .assertComplete();

        tokenStore.tokenSender(token -> rpgRetrofit.getCharters(token, 151, new Date(0))).test()
        .await().assertNoErrors().assertComplete();
//        tokenStore.tokenSender(token -> rpgRetrofit.getCharterById(token, 151, 1)).test()
//        .await().assertNoErrors().assertComplete();
//        tokenStore.tokenSender(token -> rpgRetrofit.checkinPrepare(token, 151, 1)).test()
//        .await().assertNoErrors().assertComplete();
//        tokenStore.tokenSender(token -> rpgRetrofit.postCheckin(token, 151, new CheckInCommand(1, 0, ""))).test()
//                .await().assertNoErrors().assertComplete();
    }
}
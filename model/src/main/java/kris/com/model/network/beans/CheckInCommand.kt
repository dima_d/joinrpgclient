package kris.com.model.network.beans

import com.google.gson.annotations.SerializedName

data class CheckInCommand(
        @SerializedName("ClaimId") internal val ClaimId: Int,
        @SerializedName("MoneyPaid") internal val MoneyPaid: Int,
        @SerializedName("CheckInTime") internal val CheckInTime: String
)
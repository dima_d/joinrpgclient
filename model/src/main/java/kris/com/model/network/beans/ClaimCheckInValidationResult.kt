package kris.com.model.network.beans

import com.google.gson.annotations.SerializedName

data class ClaimCheckInValidationResult (
        @SerializedName("ClaimId") internal val ClaimId: Int,
        @SerializedName("CheckedIn") internal val CheckedIn: Boolean,
        @SerializedName("Approved") internal val Approved: Boolean,
        @SerializedName("EverythingFilled") internal val EverythingFilled: Boolean,
        @SerializedName("CheckInPossible") internal val CheckInPossible: Boolean,
        @SerializedName("ClaimFeeBalance") internal val ClaimFeeBalance: Int,
        @SerializedName("Handouts") internal val Handouts: List<HandoutItem>
) {

    data class HandoutItem(@SerializedName("Label") internal var Label: String)
}
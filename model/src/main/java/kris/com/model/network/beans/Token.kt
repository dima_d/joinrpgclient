package kris.com.model.network.beans

import com.google.gson.annotations.SerializedName

data class Token(@SerializedName("access_token") internal var access_token: String,
                 @SerializedName("token_type") internal var token_type: String,
                 @SerializedName("expires_in") internal var expires_in: String)   {
    fun getToken(): String = "Bearer "+access_token
}
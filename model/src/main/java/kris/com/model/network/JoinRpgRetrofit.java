package kris.com.model.network;

import java.util.Date;
import java.util.List;

import io.reactivex.Observable;
import kris.com.model.network.beans.CharacterInfo;
import kris.com.model.network.beans.Charters;
import kris.com.model.network.beans.CheckIn;
import kris.com.model.network.beans.CheckInCommand;
import kris.com.model.network.beans.ClaimCheckInValidationResult;
import kris.com.model.network.beans.Token;
import retrofit2.http.Body;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.POST;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface JoinRpgRetrofit{
    @FormUrlEncoded
    @POST("x-api/token")
    Observable<Token> getToken(@Field("username") String username, @Field("password") String password, @Field("grant_type") String grant_type);

    @GET("x-game-api/{projectId}/characters")
    Observable<List<Charters.CharterShort>> getCharters(@Header("Authorization") String token, @Path("projectId") int projectId, @Query("modifiedSince") Date modifiedSince);

    @GET("x-game-api/{projectId}/characters/{characterId}")
    Observable<CharacterInfo> getCharterById(@Header("Authorization") String token, @Path("projectId") int projectId, @Path("characterId") int characterId);

    @GET("x-game-api/{projectId}/checkin/allclaims")
    Observable<List<CheckIn>> getCheckins(@Header("Authorization") String token, @Path("projectId") int projectId);

    @GET("x-game-api/{projectId}/checkin/{claimId}/prepare")
    Observable<ClaimCheckInValidationResult> checkinPrepare(@Header("Authorization") String token, @Path("projectId") int projectId, @Path("claimId") int claimId);

    @POST("x-game-api/{projectId}/checkin/checkin")
    Observable<Void> postCheckin(@Header("Authorization") String token, @Path("projectId") int projectId, @Body CheckInCommand checkinCommand);

}

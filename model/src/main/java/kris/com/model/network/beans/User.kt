package kris.com.model.network.beans

import com.google.gson.annotations.SerializedName

data class User(
                @SerializedName("username")  val user: String,
                @SerializedName("password") val password: String) {
    @SerializedName("grant_type") var grantType: String = "password"
}
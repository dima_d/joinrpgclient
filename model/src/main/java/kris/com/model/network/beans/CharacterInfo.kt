package kris.com.model.network.beans

import com.google.gson.annotations.SerializedName

data class CharacterInfo(
        @SerializedName("CharacterId") internal var CharacterId: Int,
        @SerializedName("UpdatedAt") internal var UpdatedAt: String,
        @SerializedName("IsActive") internal var IsActive: Boolean,
        @SerializedName("InGame") internal var InGame: Boolean,
        @SerializedName("BusyStatus") internal var BusyStatus: Int,
        @SerializedName("Groups") internal var Groups: List<GroupHeader>,
        @SerializedName("Fields") internal var Fields: List<FieldValue>,
        @SerializedName("PlayerUserId") internal var PlayerUserId: Int
) {
    data class GroupHeader(
            @SerializedName("CharacterGroupId") internal var CharacterGroupId: Int,
            @SerializedName("CharacterGroupName") internal var CharacterGroupName: String
    )

    data class FieldValue(
            @SerializedName("ProjectFieldId") internal var ProjectFieldId: Int,
            @SerializedName("Value") internal var Value: String,
            @SerializedName("DisplayString") internal var DisplayString: String
            )
}
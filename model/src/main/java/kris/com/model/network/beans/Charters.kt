package kris.com.model.network.beans

import com.google.gson.annotations.SerializedName
import java.util.*

data class Charters(internal var charters: List<CharterShort>) {
    data class CharterShort(@SerializedName("CharacterId") internal var CharacterId: Int,
                            @SerializedName("UpdatedAt") internal var UpdatedAt: Date,
                            @SerializedName("IsActive") internal var IsActive: Boolean,
                            @SerializedName("CharacterLink") internal var CharacterLink: String)
}
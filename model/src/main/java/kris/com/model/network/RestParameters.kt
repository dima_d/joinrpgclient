package kris.com.model.network

import kris.com.model.network.beans.User

data class RestParameters(val user: User, var serverUrl: String)

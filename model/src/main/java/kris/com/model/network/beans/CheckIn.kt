package kris.com.model.network.beans

import com.google.gson.annotations.SerializedName


data class CheckIn(
        @SerializedName("ClaimId") var claimId: Int = 0,
        @SerializedName("CharacterName") var characterName: String? = null,
        @SerializedName("Player") var player: Player? = null
) {
    data class Player(
            @SerializedName("PlayerId") var playerId: Int = 0,
            @SerializedName("NickName") var nickName: String? = null,
            @SerializedName("FullName") var fullName: String? = null,
            @SerializedName("OtherNicks") var otherNicks: String? = null
    )
}

package kris.com.model.token;

import io.reactivex.Observable;
import kris.com.model.network.beans.Token;
import retrofit2.http.Field;

public interface ITokenAdapter {
    Observable<Token> getToken(@Field("username") String username, @Field("password") String password, @Field("grant_type") String grant_type);
}
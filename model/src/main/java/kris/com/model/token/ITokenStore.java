package kris.com.model.token;

import io.reactivex.Observable;
import io.reactivex.ObservableSource;
import io.reactivex.functions.Function;

interface ITokenStore {
    <R> Observable<R> tokenSender(Function<String, ? extends ObservableSource<R>> mapper);
    Observable<Boolean> isConnected();
}

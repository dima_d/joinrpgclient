package kris.com.model.token;

import io.reactivex.Observable;
import io.reactivex.ObservableSource;
import io.reactivex.functions.Function;
import io.reactivex.subjects.BehaviorSubject;
import kris.com.model.network.beans.Token;
import kris.com.model.network.beans.User;

public class TokenStore implements ITokenStore {

    String SERVER_KEYWORD = "Bearer ";
    private final ITokenAdapter tokenAdapter;
    private final User user;
    private BehaviorSubject<Boolean> connected = BehaviorSubject.createDefault(false);
    private BehaviorSubject<Token> token = BehaviorSubject.create();

    public TokenStore(ITokenAdapter tokenAdapter, User user) {
        this.tokenAdapter = tokenAdapter;

        this.user = user;
    }

    @Override
    public <R> Observable<R> tokenSender(Function<String, ? extends ObservableSource<R>> mapper) {
        return connected
                .take(1)
                .switchMap(this::requestToken)
                .map(Token::getToken)
                .switchMap(mapper)
                .doOnError(err -> connected.onNext(false))
                .retry(1)
                ;

    }

    private ObservableSource<? extends Token> requestToken(Boolean connected) {
        if (connected)
            return token.take(1);
        return tokenAdapter.getToken(user.getUser(), user.getPassword(), user.getGrantType())
                .doOnNext(this.token::onNext)
                .doOnNext(token -> this.connected.onNext(true))
                .doOnError(token -> this.connected.onNext(false));
    }

    @Override
    public Observable<Boolean> isConnected() {
        return connected.distinctUntilChanged();
    }

}

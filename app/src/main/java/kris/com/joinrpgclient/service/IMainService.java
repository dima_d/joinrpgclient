package kris.com.joinrpgclient.service;

import android.support.v4.app.Fragment;

public interface IMainService {

    void showFragment(Fragment fragment, int titleId);
}

package kris.com.joinrpgclient.service;

import org.jetbrains.annotations.NotNull;

import kris.com.joinrpgclient.activity.IFabView;
import kris.com.utils.presenter.Presenter;

public class FabPresenter extends Presenter {

    FabPresenter(@NotNull MainService interactor) {
    }

    void bindView(@NotNull IFabView view){
        view.setFabVisibility(false);

    }

    public void onFabClick(){

    }
}

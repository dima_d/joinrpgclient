package kris.com.joinrpgclient.service;

import android.support.v4.app.Fragment;

import org.jetbrains.annotations.NotNull;

import io.reactivex.subjects.BehaviorSubject;
import kris.com.joinrpgclient.activity.IMainView;
import kris.com.utils.presenter.Presenter;

public class MainPresenter extends Presenter {

    @NotNull public FabPresenter fabPresenter;
    @NotNull
    private final MainService interactor;
    private BehaviorSubject<Fragment> currentFragment = BehaviorSubject.create();
    private BehaviorSubject<Integer> title = BehaviorSubject.create();

    MainPresenter(@NotNull MainService interactor) {
        fabPresenter = new FabPresenter(interactor);

        this.interactor = interactor;
    }

    public void bindView(@NotNull IMainView view){
        fabPresenter.bindView(view);

        currentFragment
                .compose(bindToViewLifecycle(view))
                .doOnNext(view::showFragment)
                .onErrorResumeNext(observer -> {})
                .subscribe();

        interactor.getFeatureManager().activeFeatures()
                .compose(bindToViewLifecycle(view))
                .doOnNext(view::setActiveFeatures)
                .onErrorResumeNext(observer -> {})
                .subscribe();

        title
                .compose(bindToViewLifecycle(view))
                .doOnNext(view::setTitle)
                .onErrorResumeNext(observer -> {})
                .subscribe();

    }

    public void onNavigationItemSelected(int itemId) {
        interactor.getFeatureManager().onFeatureCalled(itemId);
    }

    @Override
    public void stop() {
        super.stop();
        currentFragment.onComplete();
        title.onComplete();
    }

    @NotNull
    public FabPresenter getFabPresenter() {
        return fabPresenter;
    }

    void showFragment(Fragment fragment, int titleId) {
        currentFragment.onNext(fragment);
        title.onNext(titleId);
    }
}

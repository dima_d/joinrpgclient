package kris.com.joinrpgclient.service;

import android.support.v4.app.Fragment;

import org.jetbrains.annotations.NotNull;

import kris.com.joinrpgclient.features.FeatureManager;
import kris.com.utils.service.BoundService;

public class MainService extends BoundService implements IMainService {


    public static final int DISCONNECTED = 0;
    public static final int CONNECTED = 2;
    public static final int CONNECTING = 1;

    @NotNull MainPresenter mainPresenter = new MainPresenter(this);
    @NotNull FeatureManager featureManager = new FeatureManager(this);

    @Override
    public void showFragment(Fragment fragment, int titleId) {
        mainPresenter.showFragment(fragment, titleId);
    }

    ///////////////////////////////////////////////////////////////////////////
    // Getters
    ///////////////////////////////////////////////////////////////////////////

    @NotNull
    public MainPresenter getMainPresenter() {
        return mainPresenter;
    }

    @NotNull
    public FeatureManager getFeatureManager() {
        return featureManager;
    }
}

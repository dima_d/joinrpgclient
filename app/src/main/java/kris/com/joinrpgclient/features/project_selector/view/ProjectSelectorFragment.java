package kris.com.joinrpgclient.features.project_selector.view;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import org.jetbrains.annotations.NotNull;

import kris.com.joinrpgclient.R;
import kris.com.joinrpgclient.features.project_selector.ProjectSelectorFeature;

public class ProjectSelectorFragment extends Fragment implements IProjectSelectorFragment {

    private ProjectSelectorFeature interactor;

    public void setInteractor(@NotNull ProjectSelectorFeature interactor) {
        this.interactor = interactor;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_project_selection, container, false);

        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        interactor.getProjectSelectorPresenter().bindView(this);
    }

    @Override
    public void onPause() {
        super.onPause();
        interactor.getProjectSelectorPresenter().unbindView(this);
    }
}

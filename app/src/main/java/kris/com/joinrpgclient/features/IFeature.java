package kris.com.joinrpgclient.features;

import kris.com.joinrpgclient.service.IMainService;

public interface IFeature {
    void start (IMainService interactor);
    void finish();
    void onFeatureCall();
    int getMenuId();

}

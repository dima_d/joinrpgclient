package kris.com.joinrpgclient.features.settings

import android.os.Bundle
import android.support.v7.preference.PreferenceFragmentCompat
import kris.com.joinrpgclient.R

class SettingsFragment : PreferenceFragmentCompat() {
    override fun onCreatePreferences(savedInstanceState: Bundle?, rootKey: String?) {
        addPreferencesFromResource(R.xml.preferences)
    }

}

package kris.com.joinrpgclient.features;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import io.reactivex.Observable;
import io.reactivex.subjects.BehaviorSubject;
import kris.com.joinrpgclient.features.project_selector.ProjectSelectorFeature;
import kris.com.joinrpgclient.features.settings.SettingsFeature;
import kris.com.joinrpgclient.service.MainService;

public class FeatureManager {

    @NotNull private final MainService interactor;
    List<Integer> activeFeatures = new ArrayList<>();
    BehaviorSubject<List<Integer>> activeFeatureSubject = BehaviorSubject.create();

    private static final Map<Integer, IFeature> baseMap= new FeatureMap(2){{
        put(new SettingsFeature());
        put(new ProjectSelectorFeature());
    }};

    public FeatureManager(@NotNull MainService interactor) {
        this.interactor = interactor;

        for (IFeature feature : baseMap.values()){
            feature.start(interactor);
        }

        setActiveList(baseMap.keySet());
    }

    public void finish(){
        for (IFeature feature : baseMap.values()){
            feature.finish();
        }
        baseMap.clear();

    }

    private void setActiveList(Collection<Integer> list) {
        activeFeatures.clear();
        activeFeatures.addAll(list);
        activeFeatureSubject.onNext(activeFeatures);
    }

    public Observable<List<Integer>> activeFeatures(){
        return activeFeatureSubject;
    }

    public void onFeatureCalled(int itemId) {
        IFeature feature = baseMap.get(itemId);
        if (feature!=null){
            feature.onFeatureCall();
        }


    }

    private static class FeatureMap extends ConcurrentHashMap<Integer, IFeature> {

        FeatureMap(int i) {
            super(i);
        }

        void put(IFeature feature){
            put(feature.getMenuId(), feature);
        }
    }
}

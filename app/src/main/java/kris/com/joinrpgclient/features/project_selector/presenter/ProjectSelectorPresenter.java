package kris.com.joinrpgclient.features.project_selector.presenter;

import kris.com.joinrpgclient.features.project_selector.view.IProjectSelectorFragment;
import kris.com.joinrpgclient.service.IMainService;
import kris.com.utils.presenter.Presenter;

public class ProjectSelectorPresenter extends Presenter {

    public ProjectSelectorPresenter(IMainService interactor) {

    }

    public void bindView(IProjectSelectorFragment projectSelectorFragment) {

    }
}

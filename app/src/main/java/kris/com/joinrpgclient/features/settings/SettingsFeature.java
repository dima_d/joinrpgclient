package kris.com.joinrpgclient.features.settings;

import kris.com.joinrpgclient.R;
import kris.com.joinrpgclient.features.IFeature;
import kris.com.joinrpgclient.service.IMainService;

public class SettingsFeature implements IFeature {

    private IMainService interactor;

    @Override
    public void start(IMainService interactor) {
        this.interactor = interactor;
    }

    @Override
    public void finish() {
    }

    @Override
    public void onFeatureCall() {
        SettingsFragment fragment = new SettingsFragment();
        interactor.showFragment(fragment, R.string.action_settings);

    }

    @Override
    public int getMenuId() {
        return R.id.action_settings;
    }

}

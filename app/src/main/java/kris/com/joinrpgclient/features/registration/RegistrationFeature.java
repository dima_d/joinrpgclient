package kris.com.joinrpgclient.features.registration;

import kris.com.joinrpgclient.features.IFeature;
import kris.com.joinrpgclient.service.IMainService;

public class RegistrationFeature implements IFeature {

    @Override
    public void start(IMainService interactor) {
    }

    @Override
    public void finish() {

    }

    @Override
    public void onFeatureCall() {

    }

    @Override
    public int getMenuId() {
        return 0;
    }
}

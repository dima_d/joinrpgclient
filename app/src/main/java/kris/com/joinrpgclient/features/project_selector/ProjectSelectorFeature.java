package kris.com.joinrpgclient.features.project_selector;

import kris.com.joinrpgclient.R;
import kris.com.joinrpgclient.features.IFeature;
import kris.com.joinrpgclient.features.project_selector.presenter.ProjectSelectorPresenter;
import kris.com.joinrpgclient.features.project_selector.view.ProjectSelectorFragment;
import kris.com.joinrpgclient.service.IMainService;

public class ProjectSelectorFeature implements IFeature {

    private ProjectSelectorPresenter projectSelectorPresenter;
    private IMainService interactor;

    @Override
    public void start(IMainService interactor) {
        projectSelectorPresenter = new ProjectSelectorPresenter(interactor);
        this.interactor = interactor;
    }

    @Override
    public void finish() {
        projectSelectorPresenter.stop();
    }

    @Override
    public void onFeatureCall() {
        ProjectSelectorFragment fragment = new ProjectSelectorFragment();
        fragment.setInteractor(this);
        interactor.showFragment(fragment, R.string.selectProject);

    }

    @Override
    public int getMenuId() {
        return R.id.action_project_selection;
    }

    public ProjectSelectorPresenter getProjectSelectorPresenter() {
        return projectSelectorPresenter;
    }
}

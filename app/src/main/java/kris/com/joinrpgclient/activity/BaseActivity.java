package kris.com.joinrpgclient.activity;

import android.support.v7.app.AppCompatActivity;

import io.reactivex.Observable;
import kris.com.joinrpgclient.service.MainService;
import kris.com.utils.service.BoundService;
import kris.com.utils.service.IServiceConnector;

public class BaseActivity extends AppCompatActivity {

    private Observable<MainService> mainService;
    private IServiceConnector<MainService> connector;

    @Override
    protected void onResume() {
        connector = BoundService.connector(this, MainService.class);
        mainService = connector.getService();
        super.onResume();
    }

    @Override
    protected void onPause() {
        connector.complete();
        super.onPause();
    }

    public Observable<MainService> getMainService() {
        return mainService;
    }

}

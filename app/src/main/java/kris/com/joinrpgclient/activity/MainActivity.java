package kris.com.joinrpgclient.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import kris.com.joinrpgclient.R;
import kris.com.joinrpgclient.service.FabPresenter;
import kris.com.joinrpgclient.service.MainPresenter;
import kris.com.joinrpgclient.service.MainService;

public class MainActivity extends BaseActivity implements IMainView{

    private static final String TAG = MainActivity.class.getSimpleName();

    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.fab)
    FloatingActionButton fab;
    @BindView(R.id.nav_view)
    NavigationView navView;
    @BindView(R.id.drawer_layout)
    DrawerLayout drawerLayout;
    @BindView(R.id.contentFragment)
    FrameLayout contentFragment;
    private TextView summaryView;
    private FragmentManager.OnBackStackChangedListener selectFirstVisibleItem = this::selectFirstVisibleItem;;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        setSupportActionBar(toolbar);

        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawerLayout, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawerLayout.addDrawerListener(toggle);
        toggle.syncState();

        navView.setNavigationItemSelectedListener(this::onNavigationItemSelected);

        this.startService(new Intent(this, MainService.class));


        summaryView = (TextView) navView.getHeaderView(0).findViewById(R.id.summary);
    }

    private void selectFirstVisibleItem() {
        if (getSupportFragmentManager().getBackStackEntryCount() >0)
            return;

        navView.getMenu();
        Menu menu = navView.getMenu();

        for(int i = 0; i < menu.size(); i++){
            MenuItem item = menu.getItem(i);
            if (item.isVisible()){
                item.setChecked(true);
                onNavigationItemSelected(item);
                return;
            }
        }


    }

    @Override
    protected void onResume() {
        super.onResume();
        bindView();
        selectFirstVisibleItem();
        getSupportFragmentManager().addOnBackStackChangedListener(selectFirstVisibleItem);
    }

    protected void bindView() {
        getMainService()
                .map(MainService::getMainPresenter)
                .doOnNext(mainPresenter -> mainPresenter.bindView(this))
                .subscribe();
    }

    protected void unbindView() {
        getMainService()
                .map(MainService::getMainPresenter)
                .doOnNext(mainPresenter -> mainPresenter.unbindView(this))
                .subscribe();
    }

    @Override
    protected void onPause() {
        unbindView();

        getSupportFragmentManager().removeOnBackStackChangedListener(selectFirstVisibleItem);
        super.onPause();
    }

    @Override
    public void onBackPressed() {
        if (drawerLayout.isDrawerOpen(GravityCompat.START)) {
            drawerLayout.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
//            case R.id.action_settings:
//                return true;
            default:

                return super.onOptionsItemSelected(item);
        }
    }

    @SuppressWarnings("StatementWithEmptyBody")
    public boolean onNavigationItemSelected(MenuItem item) {
        getMainService()
                .map(MainService::getMainPresenter)
                .doOnNext(mainPresenter -> mainPresenter.onNavigationItemSelected(item.getItemId()))
                .subscribe();



        drawerLayout.closeDrawer(GravityCompat.START);
        return true;
    }

    ///////////////////////////////////////////////////////////////////////////
    // View
    ///////////////////////////////////////////////////////////////////////////

    @Override
    public void onError(Exception e) {
        Toast.makeText(this, e.getMessage(), Toast.LENGTH_LONG).show();
        Log.e(TAG, "onError: ", e);
    }

    @Override
    public void showFragment(Fragment fragment) {
        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.contentFragment, fragment)
                .addToBackStack(null)
                .commit();

    }

    @Override
    public void setActiveFeatures(List<Integer> features) {
        Menu menu = navView.getMenu();

        for(int i = 0; i < menu.size(); i++){
            MenuItem item = menu.getItem(i);
            if (features.contains(item.getItemId())){
                item.setVisible(true);
            } else {
                item.setVisible(false);
            }

        }
    }

    ///////////////////////////////////////////////////////////////////////////
    // FAB
    ///////////////////////////////////////////////////////////////////////////
    @OnClick(R.id.fab)
    void onFabClick(View view) {
        getMainService()
                .map(MainService::getMainPresenter)
                .map(MainPresenter::getFabPresenter)
                .doOnNext(FabPresenter::onFabClick)
                .subscribe();
    }

    @Override
    public void setFabVisibility(boolean visible) {
        fab.setVisibility(visible?View.VISIBLE:View.GONE);
    }
}

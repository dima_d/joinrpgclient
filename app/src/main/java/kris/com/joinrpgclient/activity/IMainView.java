package kris.com.joinrpgclient.activity;

import android.support.v4.app.Fragment;

import java.util.List;

import kris.com.utils.view.IView;

public interface IMainView extends IView, IFabView{

    void showFragment(Fragment fragment);
    void setActiveFeatures(List<Integer> features);

    void setTitle(int titleId);
}

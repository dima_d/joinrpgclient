package kris.com.joinrpgclient;

import org.junit.Test;

import java.util.HashMap;
import java.util.Map;

import static org.junit.Assert.*;

public class ExampleUnitTest {

    Map<Integer, Integer> testMap = new HashMap<>();

    @Test
    public void addition_isCorrect() throws Exception {

        testMap.put(1,1);
        assertEquals(testMap.values().size(), 1);
        testMap.put(1,3);
        assertEquals(testMap.values().size(), 1);
        testMap.put(2,2);
        assertEquals(testMap.values().size(), 2);
        testMap.remove(2);
        assertEquals(testMap.values().size(), 1);
        testMap.clear();
        assertEquals(testMap.values().size(), 0);



    }
}